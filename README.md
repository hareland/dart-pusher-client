# dartpusherclient

A simple dart package to handle pusher client side.

## Getting Started


```dart
import 'dartpusherclient.dart';

void main() {
  Pusher p = Pusher('APP_KEY', cluster: 'eu');

  p.connect().then((Pusher pu) {
    pu.subscribe('my-channel').listen('test', (PusherEvent e) {
      print("Received: ${e.event} in ${e.channel} with data ${e.data}");
    }).listen('away', (PusherEvent e) {
      print("Received: ${e.event} in ${e.channel} with data ${e.data}");
    });
  });
}
```



## Contributions
Contributions are most welcome.


## Roadmap:

| Feature           | Since |
|-------------------|-------|
| Channels          | 0.0.1 |
| Private Channels  | ?     |
| Presence Channels | ?     |