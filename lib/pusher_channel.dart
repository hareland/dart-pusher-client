import 'package:dartpusherclient/channel.dart';

import 'definitions.dart';
import 'pusher.dart';

///Pusher subscription, allows listening to events in a channel.
class PusherChannel extends Channel {
  PusherChannel(Pusher pusher, String name) : super(pusher, name);
}
