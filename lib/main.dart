import 'package:dartpusherclient/pusher.dart';
import 'package:dartpusherclient/pusher_event.dart';

void main() {
  Pusher p = Pusher('c87429902e85ec2c8a9f');

  p.connect().then((Pusher pu) {
    pu.subscribe('my-channel').listen('test', (PusherEvent e) {
      print("Received: ${e.event} in ${e.channel} with data ${e.data}");
    }).listen('away', (PusherEvent e) {
      print("Received: ${e.event} in ${e.channel} with data ${e.data}");
    });
  });
}
