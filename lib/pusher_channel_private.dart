import 'package:dartpusherclient/dartpusherclient.dart';

class PusherPrivateChannel extends PusherChannel {
  PusherPrivateChannel(Pusher pusher, String name) : super(pusher, name){
    print("Private channels are not yet supported by this client.");

  }

  //TODO: Implement private channels
  void authorize(String socketId, Function authorizerCallback) {}
}
