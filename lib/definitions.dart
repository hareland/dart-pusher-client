import 'pusher_event.dart';

typedef void PusherCallback(PusherEvent event);
typedef void PusherConnectionOnDoneCallback();
typedef void PusherConnectionOnErrorCallback(dynamic e);
