import 'dart:async';
import 'dart:io';

import 'package:dartpusherclient/definitions.dart';

import 'pusher.dart';

class Connection {
  WebSocket socket;
  Pusher pusher;
  String _client = 'dart-pusher';
  double _version = 1.0;
  int _protocol = 7;

  bool get open {
    return socket?.readyState == WebSocket.open;
  }

  Connection(this.pusher);

  ///Get the WS(S) URL.
  String get wsUrl {
    return "${pusher.options.forceTls ? 'wss' : 'ws'}://ws-${pusher.options.cluster}.pusher.com:${pusher.options.forceTls ? 443 : 80}/app/${pusher.options.appKey}?client=$_client&version=$_version&protocol=$_protocol";
  }

  ///Connect to Pusher
  Future<Connection> connect() async {
    if (socket != null) {
      this.close();
    }
    socket = await WebSocket.connect(wsUrl);

    if (socket?.readyState == WebSocket.closed) {
      throw new Exception("WebSocket Error: Connection is closed!");
    }

    if (open) {
      return this;
    }

    throw new Exception("WebSocket Error: Connection not open?!");
  }

  ///Send payload to server
  void write(dynamic body) {
    if (!open) {
      throw new Exception("Error: Socket is not open!");
    }
    socket.add(body);
  }

  ///Listen to any data
  StreamSubscription listen(
    Function callback, {
    PusherConnectionOnErrorCallback onError,
    PusherConnectionOnDoneCallback onDone,
    bool cancelOnError = false,
  }) {
    if (socket?.readyState == WebSocket.open) {
      return socket.listen(
        (data) {
          callback(data);
        },
        onError: (e) {
          onError(e);
        },
        onDone: () {
          onDone();
        },
        cancelOnError: cancelOnError,
      );
    }
  }

  ///Close connection.
  Future<bool> close() async {
    return await socket.close() as bool;
  }

  void dispose() {
    close();
  }
}
