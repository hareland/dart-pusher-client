import 'definitions.dart';
import 'pusher.dart';

///Pusher subscription, allows listening to events in a channel.
class Channel {
  String name;
  Pusher pusher;

  Channel(this.pusher, this.name) {
    if (pusher.channels[name] == null) {
      pusher.channels[name] = {};
    }
  }

  ///Listen to an event in this channel/subscription
  Channel listen(String event, PusherCallback callback) {
    this.on(event, callback);
    return this;
  }

  void on(String event, PusherCallback callback) {
    pusher.channels[name][event] = callback;
  }

  void unsubscribe() {
    pusher.unsubscribe(name);
  }
}
