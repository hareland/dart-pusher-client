import 'dart:convert';
import 'connection.dart';
import 'definitions.dart';
import 'pusher_event.dart';
import 'pusher_logger.dart';
import 'pusher_options.dart';
import 'pusher_channel.dart';

///Simple Pusher client
class Pusher {
  bool cancelOnError = true;
  String socketId;
  Map<String, PusherCallback> listeners = {};
  Map<String, Map<String, PusherCallback>> channels = {};
  PusherOptions options = PusherOptions();
  PusherLogger logger = PusherLogger();
  Connection connection;

  Pusher(
    String appKey, {
    String authEndpoint,
    String cluster = 'eu',
    bool forceTls = false,
    this.cancelOnError = true,
  }) {
    this.options.appKey = appKey;
    this.options.authEndpoint = authEndpoint;
    this.options.forceTls = forceTls;
    this.options.cluster = cluster;
  }

  ///Connect to pusher, when connected resolve the future.
  Future<Pusher> connect() async {
    if (connection is Connection && connection.open) {
      return this;
    }
    connection = Connection(this);
    await connection.connect();

    connection.listen(_globalEventListener,
        onDone: _globalOnDone,
        onError: _globalOnError,
        cancelOnError: cancelOnError);

    return this;
  }

  ///Create a new subscription, will allow for further listening inside a channel
  ///Supports nested events.
  PusherChannel subscribe(String channel) {
    connection.write(json.encode({
      'event': 'pusher:subscribe',
      'data': {'channel': channel}
    }));

    return PusherChannel(this, channel);
  }

  ///Unsubscribe form a channel
  void unsubscribe(String channel) {
    connection.write(json.encode({
      'event': 'pusher:unsubscribe',
      'data': {'channel': channel}
    }));
  }

  ///Disconnect from connection and unsubscribe all channels
  Future<dynamic> disconnect() async {
    channels.keys.forEach((channel) => unsubscribe(channel));
    return await connection.close();
  }

  ///Listen to a simple event, not recommended according to proto. docs:
  ///@see: https://pusher.com/docs/channels/using_channels/channels
  Pusher listen(String eventName, PusherCallback callback) {
    listeners[eventName] = callback;
    return this;
  }

  ///Listens to ANY event emitted from Pusher,
  ///Will send the event to the correct handler.
  void _globalEventListener(data) {
    PusherEvent event = PusherEvent.fromString(data);
    if (channels.containsKey(event.channel) &&
        channels[event.channel].containsKey(event.event)) {
      channels[event.channel][event.event](event);

      //TODO: Should this feature be removed?
    } else if (listeners.containsKey(event.event)) {
      listeners[event.event](event);
    } else if (_isInternalEvent(event)) {
      _handleInternalPusherEvent(event);
    } else {
      logger.log("Unknown event: ${event.toString()}");
    }
  }

  bool _isInternalEvent(PusherEvent event) {
    return event.event.startsWith('pusher:') ||
        event.event.startsWith('pusher_internal');
  }

  ///Dispose of the instance
  void dispose() {
    this.disconnect();
  }

  ///
  _globalOnError(e) {
    logger.log("ERROR:");
    logger.log(e);
    if (cancelOnError) {
      logger.log("cancelOnError = true: Closing connection.");
      this.disconnect();
    }
  }

  ///Whenever we are completely done or the server closes the connection
  PusherConnectionOnDoneCallback _globalOnDone() {
    this.disconnect();
  }

  ///Handles internal events from pusher.
  ///This needs to be extended to support more internal events.
  void _handleInternalPusherEvent(PusherEvent event) {
    switch (event.event) {
      //Pusher confirms that the connection is good.
      case 'pusher:connection_established':
        //MAke sure to remember socketID
        socketId = event.data['socket_id'];
        logger.log("Connection established. \nSocketID: $socketId");
        break;

      //We will receive events from this point forward.
      case 'pusher_internal:subscription_succeeded':
        logger.log("Subscribed to channel: ${event.channel}");
        break;

      //Send pong...
      case 'pusher:ping':
        logger.log("Pusher requested ping...");
        connection.write(json.encode({'event': 'pusher:pong'}));
        break;
      default:
        logger.log("Unknown pusher event: $event");
    }
  }
}
