import 'package:dartpusherclient/pusher.dart';
import 'package:dartpusherclient/pusher_channel_private.dart';

import 'pusher_event.dart';

class PusherPresenceChannel extends PusherPrivateChannel {
  PusherPresenceChannel(Pusher pusher, String name) : super(pusher, name) {
    print("Presence channels are not yet supported by this client.");
  }

  ///TODO: Implement
  void handleEvent(PusherEvent event) {}

  void handleInternalEvent(PusherEvent event) {}

  void handleSubscriptionSucceededEvent(PusherEvent event) {}
}
