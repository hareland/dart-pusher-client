
import 'dart:convert';

///An event received from Pusher.
class PusherEvent {
  String event;
  dynamic data;
  String channel;
  String userId;

  PusherEvent(this.event, {this.data, this.channel, this.userId});

  ///Construct the event from a raw string
  factory PusherEvent.fromString(String event) =>
      PusherEvent.fromMap(json.decode(event));

  factory PusherEvent.fromMap(Map map) {
    return PusherEvent(
      map['event'],
      //Pusher uses double encoding, to be sure we support real maps as well
      //https://pusher.com/docs/channels/library_auth_reference/pusher-websockets-protocol#double-encoding
      data: map['data'] is String
          ? json.decode(map['data'])
          : map['data'] is Map ? map['data'] : null,
      channel: map['channel'] ?? null,
      userId: map['user_id'],
    );
  }

  Map toMap() {
    return {
      'event': event,
      'data': data,
      'channel': channel,
      'user_id': userId,
    };
  }

  String toString() {
    return json.encode(toMap());
  }
}
