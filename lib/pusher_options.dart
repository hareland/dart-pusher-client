class PusherOptions {
  String appKey;
  String authEndpoint = '';
  bool forceTls = true;
  String cluster = 'eu';
}
